# 2022/03/26 (Saturday)

I finally unpacked Used-PC delivered on Monday, and installed Ubuntu Desktop 21.10.  
KVM will be setup next week, maybe...

My Old-PC (using before) was taken to partner. 

Partner's Windows PC isn't working well, and "Identity V" crash frequently.  
So, Partner started installing Windows 10 to play "Identity V".

Partner got New-PC for free. Congratulation!

## Install docker on Ubuntu using Snap

https://snapcraft.io/docker

Easier than APT. :thumbsup:

```shell
$ sudo snap install docker
$ docker --version
Docker version 20.10.12, build e91ed5707e
```

```shell
$ sudo addgroup --system docker
$ sudo adduser $USER docker
$ newgrp docker
$ sudo snap disable docker
$ sudo snap enable docker
```

Besides, Docker Compose is also installed, both V1 and V2.  
Pretty cool!! :smile:

```shell
$ docker-compose --version
docker-compose version 1.29.2, build unknown

$ docker compose --version
Docker version 20.10.12, build e91ed5707e
```

## This diary uses "Go-diary"

https://github.com/komem3/go-diary

First, Go install.

```shell
$ sudo snap install go --classic

$ go version
go version go1.17.8 linux/amd64
```

```shell
$ go env | grep -E 'PATH'
GOPATH="/home/user/go"
``` 

```shell
$ echo "export PATH=$PATH:$(go env GOPATH)/bin" >> ~/.bash_profile
$ source ~/.bash_profile 
```

And, "Go-diary" install.

```shell
$ go install github.com/komem3/go-diary/cmd/diary@latest

$ diary --version
diary version 1.4.1
```

I will do my best not to be a quitter.:sweat_smile:
